package org.aifgi.crawler

import org.aifgi.crawler.impl.CrawlerImpl
import java.net.URL
import java.io.IOException
import java.util.concurrent.Executors
import java.util.concurrent.Semaphore
import java.util.logging.Logger
import java.util.logging.Level
import org.aifgi.crawler.impl.HostFilter
import org.aifgi.crawler.api.CrawlerException
import org.aifgi.crawler.impl.CrawlerBuilder
import org.aifgi.crawler.api.LinkHolder
import org.aifgi.crawler.impl.LinkHolderImpl
import org.aifgi.crawler.impl.PageLoaderImpl
import org.aifgi.crawler.impl.LinkSearcher
import org.aifgi.crawler.impl.PageSaver
import org.aifgi.crawler.impl.MongoDBPageSaver
import org.aifgi.crawler.api.Crawler
import java.util.concurrent.ForkJoinPool
import org.aifgi.crawler.impl.ForkJoinPoolInstance
import java.util.concurrent.ForkJoinTask

/**
 * @author aifgi
 */

fun main(args: Array<String>) {
    val crawler = createCrawler("ru.wikipedia.org")
    crawler.start()
    Thread.sleep(1000000)
}

fun createCrawler(host: String): Crawler {
    val linkHolder = LinkHolderImpl()
    return CrawlerBuilder()
            .setLinkHolder(linkHolder)
            .setPageLoader(PageLoaderImpl())
            .addUrlFilter(HostFilter(host))
            .addStartPage(URL("http", host, "/"))
            .addPageHandler(LinkSearcher(linkHolder))
            .addPageHandler(PageSaver(MongoDBPageSaver()))
            .build()
}
