package org.aifgi.crawler.impl

import org.aifgi.crawler.api.LinkHolder
import java.net.URL
import org.aifgi.crawler.api.Filter
import org.aifgi.crawler.api.PageLoader
import org.aifgi.crawler.api.PageHandler
import org.aifgi.crawler.api.Crawler
import java.util.ArrayList

/**
 * @author Alexey.Ivanov
 */
public class CrawlerBuilder {
    private var linkHolder: LinkHolder? = null
    private var pageLoader: PageLoader? = null
    private val pageHandlers: MutableList<PageHandler> = ArrayList()
    private var startPageExist = false

    public fun setLinkHolder(linkHolder: LinkHolder): CrawlerBuilder {
        this.linkHolder = linkHolder
        return this
    }

    public fun setPageLoader(pageLoader: PageLoader): CrawlerBuilder {
        this.pageLoader = pageLoader
        return this
    }

    public fun addPageHandler(handler: PageHandler): CrawlerBuilder {
        pageHandlers.add(handler)
        return this
    }

    public fun addStartPage(url: URL): CrawlerBuilder {
        if (linkHolder == null) {
            throw CrawlerBuildingException("Link holder should be create before start page addition")
        }
        startPageExist = true
        linkHolder!!.addLink(url)
        return this
    }

    public fun addUrlFilter(filter: Filter<URL>): CrawlerBuilder {
        if (linkHolder == null) {
            throw CrawlerBuildingException("Link holder should be create before filter addition")
        }
        linkHolder!!.addFilter(filter)
        return this
    }

    public fun build(): Crawler {
        if (linkHolder == null) {
            throw CrawlerBuildingException("Link holder is null")
        }
        if (pageLoader == null) {
            throw CrawlerBuildingException("Page loader is null")
        }
        if (!startPageExist) {
            throw CrawlerBuildingException("Start page has not been added")
        }
        return CrawlerImpl(linkHolder!!, pageLoader!!, pageHandlers)
    }
}