package org.aifgi.crawler.impl

import java.util.concurrent.ForkJoinPool

/**
 * @author aifgi
 */
public object ForkJoinPoolInstance {
    public val instance: ForkJoinPool = ForkJoinPool(Runtime.getRuntime().availableProcessors() * 50)
}