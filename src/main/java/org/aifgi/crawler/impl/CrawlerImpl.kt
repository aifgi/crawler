package org.aifgi.crawler.impl

import java.net.URL
import org.aifgi.crawler.api.Page
import java.util.ArrayList
import java.io.InputStreamReader
import java.io.BufferedReader
import org.aifgi.crawler.impl.LinkHolderImpl
import org.aifgi.crawler.api.LinkHolder
import java.io.BufferedWriter
import java.io.FileOutputStream
import java.io.FileWriter
import java.io.File
import java.io.InputStream
import org.aifgi.crawler.api.PageLoader
import org.aifgi.crawler.api.Filter
import java.io.IOException
import org.aifgi.crawler.api.CrawlerException
import java.util.logging.Logger
import org.aifgi.crawler.api.Crawler
import org.aifgi.crawler.api.PageHandler
import java.util.concurrent.RecursiveAction

/**
 * @author aifgi
 */
internal class CrawlerImpl(private val linkHolder: LinkHolder,
                           private val loader: PageLoader,
                           private val handlers: List<PageHandler>): Crawler {
    private val log = Logger.getLogger("CrawlerFacade");

    override fun start() {
        val loaders = 9 * ForkJoinPoolInstance.instance.getParallelism() / 10
        for (i in 1..loaders) {
            ForkJoinPoolInstance.instance.execute {
                while (true) {
                    try {
                        processNextPage()
                    }
                    catch (e: CrawlerException) {
                        log.severe(e.getMessage())
                    }
                }
            }
        }
    }

    private fun processNextPage() {
        val url = linkHolder.next()
        try {
            processPage(url)
        }
        catch (e: IOException) {
            throw CrawlerException(url, "Error while crawling page", e)
        }
    }

    private fun processPage(url: URL) {
        log.info("Process page: ${url}")
        val page = loader.loadPage(url)
        handlers.forEach {
            it.handle(page)
        }
        log.info("Page ${url} successfully processed")
    }
}