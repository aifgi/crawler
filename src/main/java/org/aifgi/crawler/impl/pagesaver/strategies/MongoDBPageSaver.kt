package org.aifgi.crawler.impl

import org.aifgi.crawler.api.PageHandler
import org.aifgi.crawler.api.Page
import com.mongodb.DB
import com.mongodb.MongoClient
import com.mongodb.DBCollection
import org.aifgi.crawler.api.PageSaverStrategy
import com.mongodb.BasicDBObject
import com.mongodb.BasicDBObjectBuilder

/**
 * @author Alexey.Ivanov
 */
public class MongoDBPageSaver: PageSaverStrategy {
    private val collection: DBCollection;

    {
        val mongoClient = MongoClient("localhost")
        val db = mongoClient.getDB("crawler")
        collection = db.getCollection("pages")!!
    }

    override fun save(page: Page) {
        val urlExternalForm = page.url.toExternalForm()
        val dbObject = BasicDBObjectBuilder
                .start()
                .add("_id", urlExternalForm)
                .add("url", urlExternalForm)
                .add("lines", page.lines)
                .get()
        collection.save(dbObject)
    }
}