package org.aifgi.crawler.api

/**
 * @author Alexey.Ivanov
 */
public trait PageSaverStrategy {
    public fun save(page: Page)
}