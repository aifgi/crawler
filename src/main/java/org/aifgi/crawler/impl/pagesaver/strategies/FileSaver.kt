package org.aifgi.crawler.impl.pagesaver.strategies

import java.io.File
import org.aifgi.crawler.api.PageSaverStrategy
import org.aifgi.crawler.api.Page
import java.io.FileWriter
import java.io.IOException
import java.io.BufferedWriter
import org.aifgi.crawler.api.CrawlerException

/**
 * @author Alexey.Ivanov
 */
public class FileSaver(val directory: File): PageSaverStrategy {
    override fun save(page: Page) {
        val url = page.url
        val file = File(directory, "${url.getHost()}${url.getFile()?.replaceAll("[/\\?]", "_")}")
        try {
            val output = BufferedWriter(FileWriter(file))
            try {
                page.lines.forEach {
                    output.write(it)
                }
            }
            finally {
                output.close()
            }
        }
        catch (e: IOException) {
            throw CrawlerException(url, "Output error", e)
        }
    }
}