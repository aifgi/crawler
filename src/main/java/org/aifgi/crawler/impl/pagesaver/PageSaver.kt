package org.aifgi.crawler.impl

import java.io.File
import java.util.concurrent.Executors
import java.io.FileWriter
import java.io.BufferedWriter
import java.io.IOException
import org.aifgi.crawler.api.Page
import org.aifgi.crawler.api.PageHandler
import org.aifgi.crawler.api.PageSaverStrategy

/**
 * @author aifgi
 */
public class PageSaver(val strategy: PageSaverStrategy): PageHandler {
    public override fun handle(page: Page) {
        ForkJoinPoolInstance.instance.execute {
            doSave(page)
        }
    }

    private fun doSave(page: Page) {
        strategy.save(page)
    }
}