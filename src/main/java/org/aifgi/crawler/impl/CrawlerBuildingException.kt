package org.aifgi.crawler.impl

/**
 * @author Alexey.Ivanov
 */
public class CrawlerBuildingException(message: String, cause: Throwable? = null): Exception(message, cause) {
}