package org.aifgi.crawler.api

/**
 * @author aifgi
 */
public trait Crawler {
    public fun start()
}